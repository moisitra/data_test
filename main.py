"""
This module contains test methods for data normalization.
"""
from typing import (
        List,
        Callable
)

import numpy as np
import matplotlib.pyplot as plt

import helpers

def normalize_value(
        value: float,
        min_value: float,
        max_value: float,
        from_explicit: float,
        to_explicit: float) -> float:
    """
    Normalizes value in range [from_explicit, to_explicit].
    """
    return (value - min_value) / (max_value - min_value) \
        * (to_explicit - from_explicit) + from_explicit

def normalize_values(
        values: List[float],
        from_explicit: float,
        to_explicit: float) -> List[float]:
    """
    Normalizes values in range [from_explicit, to_explicit].
    """
    min_value = min(values)
    max_value = max(values)
    return [normalize_value(
        value,
        min_value,
        max_value,
        from_explicit,
        to_explicit) for value in values]

RANGE_START = -5
RANGE_STOP = 5
RANGE_STEP = 0.1

FROM_EXPLICIT = -1
TO_EXPLICIT = 1

calculator: Callable[[float], float] = helpers.parabola_at

data = np.arange(RANGE_START, RANGE_STOP, RANGE_STEP)
mapped_data = normalize_values(data, FROM_EXPLICIT, TO_EXPLICIT)
calculated_values = [calculator(value) for value in data]
mapped_values = normalize_values(calculated_values, 0, 1)

helpers.randomize_with_noize(calculated_values, -0.5, 1.0)

fig, (AX_LEFT, AX_RIGHT) = plt.subplots(
        nrows=1,
        ncols=2,
        sharey=True)
AX_LEFT.plot(data, calculated_values)
AX_RIGHT.plot(mapped_data, mapped_values)
plt.show()
